﻿
namespace Сompanyname.ImageProcessing.Sergei
{
    partial class _fmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._grpWorkWithImages = new System.Windows.Forms.GroupBox();
            this._cmbImageProcessingType = new System.Windows.Forms.ComboBox();
            this._btnProcessImage = new System.Windows.Forms.Button();
            this._pbProcessedImage = new System.Windows.Forms.PictureBox();
            this._pbSourceImage = new System.Windows.Forms.PictureBox();
            this._btnImageOpen = new System.Windows.Forms.Button();
            this._grpWorkWithImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._pbProcessedImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbSourceImage)).BeginInit();
            this.SuspendLayout();
            // 
            // _grpWorkWithImages
            // 
            this._grpWorkWithImages.Controls.Add(this._cmbImageProcessingType);
            this._grpWorkWithImages.Controls.Add(this._btnProcessImage);
            this._grpWorkWithImages.Controls.Add(this._pbProcessedImage);
            this._grpWorkWithImages.Controls.Add(this._pbSourceImage);
            this._grpWorkWithImages.Controls.Add(this._btnImageOpen);
            this._grpWorkWithImages.Location = new System.Drawing.Point(12, 12);
            this._grpWorkWithImages.Name = "_grpWorkWithImages";
            this._grpWorkWithImages.Size = new System.Drawing.Size(1147, 433);
            this._grpWorkWithImages.TabIndex = 0;
            this._grpWorkWithImages.TabStop = false;
            // 
            // _cmbImageProcessingType
            // 
            this._cmbImageProcessingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cmbImageProcessingType.FormattingEnabled = true;
            this._cmbImageProcessingType.Items.AddRange(new object[] {
            "Осветление",
            "Затемнение",
            "Автоконтрастирование",
            "Оператор Собеля",
            "Оператор Превитта",
            "Повышение резкости",
            "Размытие"});
            this._cmbImageProcessingType.Location = new System.Drawing.Point(459, 294);
            this._cmbImageProcessingType.Name = "_cmbImageProcessingType";
            this._cmbImageProcessingType.Size = new System.Drawing.Size(170, 21);
            this._cmbImageProcessingType.TabIndex = 4;
            // 
            // _btnProcessImage
            // 
            this._btnProcessImage.Enabled = false;
            this._btnProcessImage.Location = new System.Drawing.Point(459, 337);
            this._btnProcessImage.Name = "_btnProcessImage";
            this._btnProcessImage.Size = new System.Drawing.Size(172, 23);
            this._btnProcessImage.TabIndex = 3;
            this._btnProcessImage.Text = "Обработать изображение";
            this._btnProcessImage.UseVisualStyleBackColor = true;
            this._btnProcessImage.Click += new System.EventHandler(this._btnProcessImage_Click);
            // 
            // _pbProcessedImage
            // 
            this._pbProcessedImage.Location = new System.Drawing.Point(713, 48);
            this._pbProcessedImage.Name = "_pbProcessedImage";
            this._pbProcessedImage.Size = new System.Drawing.Size(389, 379);
            this._pbProcessedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._pbProcessedImage.TabIndex = 2;
            this._pbProcessedImage.TabStop = false;
            // 
            // _pbSourceImage
            // 
            this._pbSourceImage.Location = new System.Drawing.Point(6, 48);
            this._pbSourceImage.Name = "_pbSourceImage";
            this._pbSourceImage.Size = new System.Drawing.Size(389, 379);
            this._pbSourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._pbSourceImage.TabIndex = 1;
            this._pbSourceImage.TabStop = false;
            // 
            // _btnImageOpen
            // 
            this._btnImageOpen.Location = new System.Drawing.Point(6, 19);
            this._btnImageOpen.Name = "_btnImageOpen";
            this._btnImageOpen.Size = new System.Drawing.Size(145, 23);
            this._btnImageOpen.TabIndex = 0;
            this._btnImageOpen.Text = "Открыть изображение";
            this._btnImageOpen.UseVisualStyleBackColor = true;
            this._btnImageOpen.Click += new System.EventHandler(this._btnImageOpen_Click);
            // 
            // _fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1162, 465);
            this.Controls.Add(this._grpWorkWithImages);
            this.Name = "_fmMain";
            this.Text = "Тестовое задание: обработка изображений";
            this.Load += new System.EventHandler(this._fmMain_Load);
            this._grpWorkWithImages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._pbProcessedImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pbSourceImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _grpWorkWithImages;
        private System.Windows.Forms.PictureBox _pbSourceImage;
        private System.Windows.Forms.Button _btnImageOpen;
        private System.Windows.Forms.PictureBox _pbProcessedImage;
        private System.Windows.Forms.Button _btnProcessImage;
        private System.Windows.Forms.ComboBox _cmbImageProcessingType;
    }
}

