﻿using System;
using System.Runtime.InteropServices;

namespace Сompanyname.ImageProcessing.Sergei
{
    /// <summary>
    /// Структура, содержащая информацию о цвете точки в виде RGB компонент и в виде массива RGB из 3х байт.
    /// Элемент массива RGB[0] соответствует полю R, RGB[1] полю G и RGB[2] полю B
    /// И к цветовым компонентам можно обращаться как по имени (R,G,B) так и как к элементу массива RGB (RGB[i], i=0,1,2),
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct ColorInRGBbytes
    {
        [FieldOffset(0)]
        public byte R;
        [FieldOffset(1)]
        public byte G;
        [FieldOffset(2)]
        public byte B;

        [FieldOffset(0)]
        public fixed byte RGB[3];
        
        public void SetRGBiWithClamp(byte i, float value) // Вместо Math.Clamp(Gnorm, 0, 255), которой здесь нет.
        {
            if (value <= 0)
                RGB[i] = 0;
            else
            if(value >= 255)
                RGB[i] = 255;
            else
                RGB[i] = (byte)value;
        }
    }
}
