﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Сompanyname.ImageProcessing.Sergei
{
    // Некоторые именования переменных по Naming Conventions не смог подобрать лучше, чем без его соблюдения
    // byte* p - название pComponent громоздковато, а как ещё.
    // BitmapData bmData - data, points, pointsOfBitmap, dataPoints мне не понравилось, лучше не придумал.
    // byte *pDst

    // Порядок следования компонент в BitmapData, для формата работа с которым реализована.
    // blue = p[0];
    // green = p[1];
    // red = p[2];

    /// <summary> Служит для считывания изображения и выполнения его обработки. </summary>
    public class MyImageHandler
    {
        private Bitmap _sourceBitmap = null;

        public bool OpenSourceImage(string fileName)
        {
            _sourceBitmap?.Dispose();

            try
            {
                _sourceBitmap = (Bitmap)Image.FromFile(fileName);

                if (_sourceBitmap.PixelFormat != PixelFormat.Format24bppRgb)
                {
                    MessageBox.Show("Обрабатываются только изображения с форматом пикселя Format24bppRgb");
                    _sourceBitmap.Dispose();
                    _sourceBitmap = null;
                    return false;
                }

                return true;
            }
            catch (OutOfMemoryException)
            {
                _sourceBitmap = null;
                MessageBox.Show("Ошибка считывания изображения");
                return false;
            }
        }

        /// <summary>Изменение яркости изображения.</summary>
        /// <param name="kBrightness">коэффициент на который будут умножаться значения цветовых составляющих всех пикселов рисунка</param>
        /// <returns>Возвращает объект Bitmap содержащий результат обработки</returns>
        public Bitmap ChangeBrightness(double kBrightness)
        {
            if (kBrightness <= 0)
            {
                throw new ArgumentException($"{nameof(kBrightness)} <= 0 в {nameof(ChangeBrightness)}");
            }

            Bitmap resultBitmap = new Bitmap(_sourceBitmap);
            BitmapData resultPointsData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            try
            {
                int addForFullLine = resultPointsData.Stride - resultBitmap.Width * 3;    // Т.к. ширина одной строки пикселей (строки развертки), округляется до 4-байтовой границы
                IntPtr Scan0 = resultPointsData.Scan0;

                unsafe
                {
                    double pixel;
                    byte* p = (byte*)Scan0;

                    for (int y = 0; y < resultBitmap.Height; ++y)
                    {
                        for (int x = 0; x < resultBitmap.Width; ++x)
                        {
                            for (byte i = 0; i < 3; ++i)    // Для кадой из rgb компонент. 
                            {
                                pixel = kBrightness * *p;
                                if (pixel > 255) pixel = 255;
                                *p = (byte)pixel;
                                ++p;
                            }
                        }
                        p += addForFullLine;
                    }
                }
            }
            finally
            {
                resultBitmap.UnlockBits(resultPointsData);
            }
            return resultBitmap;
        }

        // TO DO
        // использовать таблицу с подсчитынными значениями
        // если яркости есть во всём диапазоне - проходиться окном, найти место где максимальное значение яркостей и от этого места яркости раскидать чуть в стороны или просто выравнивать распределение яркостей
        // растягивать по каждой цветовой составляющей отдельно - но уйдёт цветовой баланс
        // функцию преобразования пикселя как делегат?

        // Осуществляется проход по всем пикселям изображения и поиск мин. и макс. значений цветовой составляющей (мин. и макс. одни на всё три rgb составляющие)
        // Далее к цветовым составляющим осуществляется линейное преобразование (одинаковое, чтобы не было изменения цветового баланса)
        // Коэффициенты преобразования подобраны так, чтобы мин. -> 0, макс. -> 255
        // Поиск min и max и и пересчёт значений составляющихдля для производительности не стал выносить в отдельные функции

        /// <summary>Автоматическое изменение контрастности путем расширения диаазона яркостей по мин. и макс. значению для всех цветовых составляющих.</summary>        
        /// <returns>Возвращает объект Bitmap содержащий результат обработки</returns>
        public Bitmap AutoContrastingByOneCoefficientForСomponents()
        {
            Bitmap resultBitmap = new Bitmap(_sourceBitmap);
            BitmapData resultPointsData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            try
            {
                int addForFullLine = resultPointsData.Stride - resultBitmap.Width * 3;    // Т.к. ширина одной строки пикселей (строки развертки), округляется до 4-байтовой границы
                IntPtr Scan0 = resultPointsData.Scan0;

                unsafe
                {
                    byte* p = (byte*)Scan0;

                    byte minV = *p,
                         maxV = *p;

                    for (int y = 0; y < resultBitmap.Height; ++y)
                    {
                        for (int x = 0; x < resultBitmap.Width; ++x)
                        {
                            for (byte i = 0; i < 3; ++i)    // Для кадой из rgb компонент. 
                            {
                                if (*p > maxV) maxV = *p;
                                if (*p < minV) minV = *p;
                                ++p;
                            }
                        }
                        p += addForFullLine;
                    }

                    if (maxV - minV > 50  // Если изображение полутоновое, а не одноцвтное. По хорошему, конечно, по распределению яркостей определять - а то скажем для двуцветных уже не отловится.
                        && maxV - minV < 250) // Иначе яркости и так распределены по всему диапазону. TO DO можно, конечно, учитывать количество точек на границах диапазонов и если их несколько штук, не учитывать.
                    {
                        p = (byte*)Scan0;

                        float a = (float)(255.0 / (maxV - minV));
                        float b = -a * minV;

                        for (int y = 0; y < resultBitmap.Height; ++y)
                        {
                            for (int x = 0; x < resultBitmap.Width; ++x)
                            {
                                for (byte i = 0; i < 3; ++i)    // Для кадой из rgb компонент.
                                {
                                    *p = (byte)(a * *p + b);
                                    ++p;
                                }
                            }
                            p += addForFullLine;
                        }
                    }
                }
            }
            finally
            {
                resultBitmap.UnlockBits(resultPointsData);
            }
            return resultBitmap;
        }

        /// <summary> Обработка изображения методами, использующими градиент. </summary>
        /// <param name="operatorMatrix">Матрица 3x3 задающая способ расчёта градиента. Исходная используется для gradX, транспонированная для gradY.</param>
        /// <returns>Возвращает объект Bitmap содержащий результат обработки.</returns>
        public Bitmap GradientProcessing(sbyte[,] operatorMatrix)
        {
            Bitmap resultBitmap = (Bitmap)_sourceBitmap.Clone();

            // bmResultData и bmSourceData лучше бы смотрелись, как по мне.
            BitmapData resultPointsData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData sourcePointData = _sourceBitmap.LockBits(new Rectangle(0, 0, _sourceBitmap.Width, _sourceBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            try
            {
                // Т.к. ширина одной строки пикселей (строки развертки), округляется до 4-байтовой границы.                
                int addForFullLine = sourcePointData.Stride - resultBitmap.Width * 3;

                IntPtr dstScan0 = resultPointsData.Scan0;   // destinationScan0 - ну не смотрится, мне кажется.

                unsafe
                {
                    byte* pDst = (byte*)dstScan0;   // Для переменной в небольшом блоке можно и такое название?.

                    for (int y = 0; y < resultBitmap.Height; ++y)
                    {
                        for (int x = 0; x < resultBitmap.Width; ++x)
                        {
                            ColorInRGBbytes color = GetGradientValueForPoint(x, y, sourcePointData, operatorMatrix);
                            *pDst++ = color.B;
                            *pDst++ = color.G;
                            *pDst++ = color.R;
                        }
                        pDst += addForFullLine;
                    }
                }
            }
            finally
            {
                resultBitmap.UnlockBits(resultPointsData);
                _sourceBitmap.UnlockBits(sourcePointData);
            }

            return resultBitmap;
        }

        // TO DO реалиизовать просто свёртку (без градиента) с матрицами и посмотреть как это выглядит

        /// <summary> Обработка изображения посредством свёртки с матрицей 3x3. </summary>
        /// <param name="operatorMatrix">Матрица 3x3 используемая для свёртки с исходным изображением. </param>
        /// <returns>Возвращает объект Bitmap содержащий результат обработки. </returns>
        public Bitmap СonvolutionWithMatrix(float[,] operatorMatrix)
        {
            Bitmap resultBitmap = (Bitmap)_sourceBitmap.Clone();
            
            BitmapData resultPointsData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData sourcePointData = _sourceBitmap.LockBits(new Rectangle(0, 0, _sourceBitmap.Width, _sourceBitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            try
            {
                // Т.к. ширина одной строки пикселей (строки развертки), округляется до 4-байтовой границы.                
                int addForFullLine = sourcePointData.Stride - resultBitmap.Width * 3;

                IntPtr dstScan0 = resultPointsData.Scan0;   // destinationScan0 - ну не смотрится, мне кажется.

                unsafe
                {
                    byte* pDst = (byte*)dstScan0;   // Для переменной в небольшом блоке можно и такое название?.

                    for (int y = 0; y < resultBitmap.Height; ++y)
                    {
                        for (int x = 0; x < resultBitmap.Width; ++x)
                        {
                            ColorInRGBbytes color = GetApplyingOperatorForPoint(x, y, sourcePointData, operatorMatrix);
                            *pDst++ = color.B;
                            *pDst++ = color.G;
                            *pDst++ = color.R;
                        }
                        pDst += addForFullLine;
                    }
                }
            }
            finally
            {
                resultBitmap.UnlockBits(resultPointsData);
                _sourceBitmap.UnlockBits(sourcePointData);
            }

            return resultBitmap;
        }

        /// <summary>
        /// Возвращает матрицу 3x3, элементы которой цветовые компоненеты для данных bmData в окрестности точки с координатами x, y.
        /// </summary>
        /// <param name="x">Координата x изображения.</param>
        /// <param name="y">Координата y изображения.</param>
        /// <param name="bmData">Объект BitmapData из данных которого выбираются элементы в результирующую матрицу. </param>
        /// <returns>Матрица 3x3, элементы которой цветовые компоненеты (объекты ColorInRGBbytes).</returns>
        private unsafe ColorInRGBbytes[,] GetNeighborhood(int x, int y, BitmapData bmData)
        {
            if (x < 1 || x >= bmData.Width - 1 || y < 1 || y >= bmData.Height - 1)
            {
                throw new ArgumentException("При вызове {nameof(GetNeighborhood)} один из параметров на границе изображения или выходит за границу.");
            }

            var result = new ColorInRGBbytes[3, 3];

            // Номер строки в первом индексе.
            result[0, 0] = GetRGBFromBitmapData(x - 1, y - 1, bmData);
            result[0, 1] = GetRGBFromBitmapData(x, y - 1, bmData);
            result[0, 2] = GetRGBFromBitmapData(x + 1, y - 1, bmData);

            result[1, 0] = GetRGBFromBitmapData(x - 1, y, bmData);
            result[1, 1] = GetRGBFromBitmapData(x, y, bmData);
            result[1, 2] = GetRGBFromBitmapData(x + 1, y, bmData);

            result[2, 0] = GetRGBFromBitmapData(x - 1, y + 1, bmData);
            result[2, 1] = GetRGBFromBitmapData(x, y + 1, bmData);
            result[2, 2] = GetRGBFromBitmapData(x + 1, y + 1, bmData);

            return result;
        }

        /// <summary>
        /// Возвращает цвет (объект ColorInRGBbytes) для точки с координатами x, y данных bmData
        /// </summary>
        /// <param name="x">x координата точки для которой возвращается результат.</param>
        /// <param name="y">y координата точки для которой возвращается результат.</param>        
        /// <returns>Объект ColorInRGBbytes, содержащий цветовые компоненты для точки с координатами x,y данных bmData.</returns>
        private unsafe ColorInRGBbytes GetRGBFromBitmapData(int x, int y, BitmapData bmData)
        {
            byte* p = (byte*)bmData.Scan0;
            p += 3 * x + y * bmData.Stride;

            var result = new ColorInRGBbytes() { B = *p++, G = *p++, R = *p };
            return result;
        }

        /// <summary>
        /// Для точки с координатами x, y в данных изображения, вычисляет модуль (длину) градиента по её окрестности, используя передаваемую матрицу 3x3.
        /// </summary>
        /// <param name="bmData">Данные из которых вычисляется модуль градиента.</param>
        /// <param name="operatorMatrix"></param>
        /// <returns>Объект ColorInRGBbytes, содержащий значения цветовых компонент модуля (длины) градиента для точки с координатами x, y.</returns>
        private unsafe ColorInRGBbytes GetGradientValueForPoint(int x, int y, BitmapData bmData, sbyte[,] operatorMatrix)
        {
            // TO DO Возможно работало бы также и если в пролутоновое сначала перевести и не для трёх компонент пройтись, а для одного полутонового?

            // Для точек на краю изображения, для простоты будем возвращать значение из исходного изображения.
            if (x == 0 || x == bmData.Width - 1 || y == 0 || y == bmData.Height - 1)
            {
                return GetRGBFromBitmapData(x, y, bmData);
            }

            var neighborhood = GetNeighborhood(x, y, bmData);

            // По отдельности для каждой из цветовых компонент считаю норму градиента.
            var Gx = new float[3];
            var Gy = new float[3];

            var result = new ColorInRGBbytes();

            for (byte i = 0; i < 3; i++)
            {
                // Расписать циклы ниже в виде суммы девяти произведений (развернуть циклы) - считалось бы быстрее, но менее наглядно для чтения.

                Gx[i] = 0;
                for (byte row = 0; row < 3; ++row)
                {
                    for (byte col = 0; col < 3; ++col)
                    {
                        Gx[i] += operatorMatrix[row, col] * neighborhood[row, col].RGB[i];
                    }
                }

                // Явным образом матрицу operatorMatrix не транспонирую, просто в выборке элементов поменял [row, col] на [col, row].
                Gy[i] = 0;
                for (byte row = 0; row < 3; ++row)
                {
                    for (byte col = 0; col < 3; ++col)
                    {
                        Gy[i] += operatorMatrix[col, row] * neighborhood[row, col].RGB[i];
                    }
                }

                var Gnorm = Math.Sqrt(Gx[i] * Gx[i] + Gy[i] * Gy[i]);
                
                result.SetRGBiWithClamp(i, (float)Gnorm);
            }
            return result;
        }

        /// <summary>
        /// Для точки с координатами x, y в данных изображения вычисляет свёртку её окрестности с матрицей 3x3 (применяет оператор).
        /// </summary>
        /// <param name="bmData">Данные к которым применяется оператор.</param>
        /// <param name="operatorMatrix">VМатрица применяемого оператора (используется для вычисления свёртки).</param>
        /// <returns>Объект ColorInRGBbytes, содержащий значения цветовых компонент точки соотв. x, y после применения оператора.</returns>
        private unsafe ColorInRGBbytes GetApplyingOperatorForPoint(int x, int y, BitmapData bmData, float[,] operatorMatrix)
        {
            // Для точек на краю изображения, для простоты будем возвращать значение из исходного изображения.
            if (x == 0 || x == bmData.Width - 1 || y == 0 || y == bmData.Height - 1)
            {
                return GetRGBFromBitmapData(x, y, bmData);
            }

            var neighborhood = GetNeighborhood(x, y, bmData);

            var result = new ColorInRGBbytes();

            for (byte i = 0; i < 3; i++)
            {
                float summForCurColor = 0;
                for (byte row = 0; row < 3; ++row)
                {
                    for (byte col = 0; col < 3; ++col)
                    {
                        summForCurColor += operatorMatrix[row, col] * neighborhood[row, col].RGB[i];
                    }
                }
                
                result.SetRGBiWithClamp(i, summForCurColor);
            }
            return result;
        }

        public Image GetSourceImageClone()
        {
            return (Image)_sourceBitmap.Clone();
        }

        /// <summary> Возвращает яркость подсчитанную из трёх цветовых компонент, по одной из моделей. </summary>        
        private static byte GetY(byte red, byte green, byte blue)
        {
            return (byte)(0.299 * red + 0.587 * green + 0.114 * blue);
        }
    }
}
