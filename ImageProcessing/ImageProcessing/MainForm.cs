﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сompanyname.ImageProcessing.Sergei
{
    public partial class _fmMain : Form
    {
        MyImageHandler myImageHandler = new MyImageHandler();

        public _fmMain()
        {
            InitializeComponent();
        }

        private void _btnImageOpen_Click(object sender, EventArgs e)
        {
            var openDialog = new OpenFileDialog
            {
                Filter = "Файлы изображений|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openDialog.ShowDialog() != DialogResult.OK) return;

            _btnProcessImage.Enabled = false;

            _pbSourceImage.Image?.Dispose();
            _pbSourceImage.Image = null;

            _pbProcessedImage.Image?.Dispose();
            _pbProcessedImage.Image = null;

            if (myImageHandler.OpenSourceImage(openDialog.FileName))
            {
                _pbSourceImage.Image = myImageHandler.GetSourceImageClone();
                _btnProcessImage.Enabled = true;
            }
        }

        private async void _btnProcessImage_Click(object sender, EventArgs e)
        {
            _btnImageOpen.Enabled = false;
            _btnProcessImage.Enabled = false;
            _pbProcessedImage.Image?.Dispose();
            _pbProcessedImage.Image = null;

            Task<Bitmap> taskProcessing;

            switch (_cmbImageProcessingType.SelectedIndex)
            {
                case 0:
                    taskProcessing = Task.Run(() => myImageHandler.ChangeBrightness(1.25));
                    break;
                case 1:
                    taskProcessing = Task.Run(() => myImageHandler.ChangeBrightness(0.75));
                    break;
                case 2:
                    taskProcessing = Task.Run(() => myImageHandler.AutoContrastingByOneCoefficientForСomponents());
                    break;
                case 3:
                    // С матрицей оператора Собеля.
                    taskProcessing = Task.Run(() => myImageHandler.GradientProcessing(new sbyte[3, 3] { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } }));
                    break;
                case 4:
                    // С матрицей оператора Преввита.
                    taskProcessing = Task.Run(() => myImageHandler.GradientProcessing(new sbyte[3, 3] { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } }));
                    break;
                case 5:
                    // Повышение резкости.
                    taskProcessing = Task.Run(() => myImageHandler.СonvolutionWithMatrix(new float[3, 3] { { -0.32f, -0.32f, -0.32f }, { -0.32f, 3.56f, -0.32f }, { -0.32f, -0.32f, -0.32f } }));
                    break;
                case 6:
                    // Сглаживание по Гауссу                    
                    taskProcessing = Task.Run(() => myImageHandler.СonvolutionWithMatrix(new float[3, 3] { { 0.0625f, 0.125f, 0.0625f }, { 0.125f, 0.25f, 0.125f }, { 0.0625f, 0.125f, 0.0625f } }));
                    break;

                default:
                    taskProcessing = Task.Run(() => new Bitmap(10, 10));
                    break;
            }

            var r = await taskProcessing;
            _pbProcessedImage.Image = r;
            _btnImageOpen.Enabled = true;
            _btnProcessImage.Enabled = true;
        }

        private void _fmMain_Load(object sender, EventArgs e)
        {
            _cmbImageProcessingType.SelectedIndex = 3;
        }
    }
}
